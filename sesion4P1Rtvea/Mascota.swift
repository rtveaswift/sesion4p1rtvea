//
//  Mascota.swift
//  tiendecita
//
//  Created by Pako on 02/02/16.
//  Copyright © 2016 Pako. All rights reserved.
//

import UIKit

protocol Mascota
{
 
    
    var nombre: String {get}
    
    var nacimiento: NSDate {get}
    
    var edad: Int {get}

    var foto: String {get}
    
}
