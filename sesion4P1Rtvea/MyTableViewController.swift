//
//  ViewController.swift
//  tiendecita
//
//  Created by Pako on 02/02/16.
//  Copyright © 2016 Pako. All rights reserved.
//

import UIKit

class MyTableViewController: UITableViewController {

    
    var mascotas:[Mascota] =
    [
        Gatito(nombre: "Pepito", nacimiento: "02-02-2015 00:00", foto: "https://placehold.it/150/54176f", raza: Gatito.RazaDeGato.Ashera, precio: 119.99),
        Gatito(nombre: "Antoñito", nacimiento: "03-04-2015 00:13", foto: "https://placehold.it/150/54176f", raza: Gatito.RazaDeGato.Bobtail, precio: 65.99),
        Gatito(nombre: "Viki", nacimiento: "12-12-2015 07:30", foto: "https://placehold.it/150/54176f", raza: Gatito.RazaDeGato.Burmés, precio: 93.25),
        Gatito(nombre: "Sisi", nacimiento: "01-01-2016 18:00", foto: "https://placehold.it/150/54176f", raza: Gatito.RazaDeGato.California, precio: 219.00),
        Gatito(nombre: "Coco", nacimiento: "07-12-2015 20:08", foto: "https://placehold.it/150/54176f", raza: Gatito.RazaDeGato.Foldex, precio: 89.69),
        Gatito(nombre: "Miki", nacimiento: "20-01-2016 03:00", foto: "https://placehold.it/150/54176f", raza: Gatito.RazaDeGato.Himalayo, precio: 99.49),
        Gatito(nombre: "Rocky", nacimiento: "03-12-2015 04:30", foto: "https://placehold.it/150/54176f", raza: Gatito.RazaDeGato.Korat, precio: 100.50)
    
    ]
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//NOT NEEDDED since we hooked them up in IB already
//         tableView.delegate = self
//        tableView.dataSource = self
//        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "gatitoCell")
        self.navigationController?.hidesBarsOnSwipe = true
        title = "gatitos"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mascotas.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "gatitoCell", for: indexPath)

        let gatito: Gatito = mascotas[indexPath.row] as! Gatito

        cell.textLabel?.text = gatito.nombre

        cell.imageView?.downloaded(fromLink: gatito.foto, contentMode: .scaleAspectFit, withSquaredSize: cell.bounds.height, andBlock: {
            tableView.reloadData()
        })
        //Why don´t we use NSCache for images? IF there´s time to do it, make it
        cell.detailTextLabel?.text = gatito.miRaza?.rawValue


        return cell
    }

  
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vci = segue.identifier
        {
            switch vci
            {
                case "FichaGatito":
                    if let vc = segue.destination as? FichaGatitoViewController
                    {
                        if let index = tableView.indexPathForSelectedRow?.row
                        {
                            vc.gatito = mascotas[index] as! Gatito
                        }
                        
                    }
                default:
                    break
                
            }
        }
    }
    
}

