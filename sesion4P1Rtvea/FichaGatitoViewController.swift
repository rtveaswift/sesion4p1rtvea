//
//  FichaGatito.swift
//  tiendecita
//
//  Created by Pako on 02/02/16.
//  Copyright © 2016 Pako. All rights reserved.
//

import UIKit

class FichaGatitoViewController: UIViewController {

    var gatito: Gatito!
    static let moneda = "€"
    
    @IBOutlet weak var costeLabel: UILabel!
    @IBOutlet weak var gatitoImagen: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
        gatitoImagen.downloaded(fromLink: gatito.foto, contentMode: .scaleAspectFit, withSquaredSize: gatitoImagen.bounds.height, andBlock: {})
        costeLabel.text = String(gatito.precio) + FichaGatitoViewController.moneda
        
        
    }
    

    @IBAction func comprarGatito(sender: AnyObject) {
        
        gatito.libre = !gatito.libre
    }
    
}

