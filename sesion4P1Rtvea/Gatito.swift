//
//  Gatito.swift
//  tiendecita
//
//  Created by Pako on 02/02/16.
//  Copyright © 2016 Pako. All rights reserved.
//

import UIKit

struct Gatito: Mascota
{
    enum RazaDeGato: String
    {
        case Burmés
        case Oriental
        case Rabón
        case Ashera
        case Bobtail
        case California
        case Foldex
        case Himalayo
        case Korat
    }
    
    let nombre: String

    
    let nacimiento: NSDate
    
    var edad: Int {
        get {
            return Int(NSDate().timeIntervalSince(nacimiento as Date)) //TODO: convertir a años
        }
    }
    
    let foto: String
    
    var libre: Bool
    
    var miRaza: RazaDeGato?
    
    let precio: Float


    
    init(nombre: String, nacimiento: String, foto: String, raza: RazaDeGato?, precio: Float)
    {
        let http = "https://"
        let dateFormatter = DateFormatter()
        var s = foto

        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"

        
        if !s.hasPrefix(http)
        {
            s = http + s
        }
        
        self.nacimiento = dateFormatter.date(from: nacimiento)! as NSDate
        self.nombre = nombre
        self.foto = s
        self.miRaza = raza
        self.libre = true
        self.precio = precio
    }

    

}
