//
//  Extensions.swift
//  tiendecita
//
//  Created by Pako on 02/02/16.
//  Copyright © 2016 Pako. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloaded(fromLink link:String, contentMode mode: UIViewContentMode, withSquaredSize height: CGFloat, andBlock:@escaping ()->()) {
        guard
            let url = NSURL(string: link)
            else {return}
        contentMode = mode
        URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) -> Void in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image.resizeToBoundingSquare(boundingSquareSideLength: height)
                andBlock()
                
            }
        }).resume()
    }
}

extension UIImage
{
    func resizeToBoundingSquare(boundingSquareSideLength : CGFloat) -> UIImage
    {
        let imgScale = self.size.width > self.size.height ? boundingSquareSideLength / self.size.width : boundingSquareSideLength / self.size.height
        let newWidth = self.size.width * imgScale
        let newHeight = self.size.height * imgScale
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContext(newSize)
        
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext();
        
        return resizedImage!
    }
    
}
